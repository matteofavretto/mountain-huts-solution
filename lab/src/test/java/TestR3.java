import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mountainhuts.Region;

public class TestR3 {
	
	@Test
	public void testReadMunicipalities() {
		Region r = Region.fromFile("Piemonte", "mountain_huts.csv");
		int municipalitiesCount = r.getMunicipalities().size();
		assertTrue(municipalitiesCount > 0);
	}
	
	@Test
	public void testReadMountainHuts() {
		Region r = Region.fromFile("Piemonte", "mountain_huts.csv");
		int mountainHutsCount = r.getMountainHuts().size();
		assertTrue(mountainHutsCount > 0);
	}
}