import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import mountainhuts.Region;

public class TestR4 {
	Region r;
	
	@Before
	public void setUp() {
		r = Region.fromFile("Piemonte", "mountain_huts.csv");
	}
	
	@Test
	public void testCountMunicipalitiesPerProvince() {
		Map<String, Long> map = r.countMunicipalitiesPerProvince();
		assertNotNull(map);
	}
	
	@Test
	public void testCountMountainHutsPerMunicipalityPerProvince() {
		Map<String, Map<String,Long>> map = r.countMountainHutsPerMunicipalityPerProvince();
		assertNotNull(map);
	}
	
	@Test
	public void testCountMountainHutsPerAltitudeRange() {
		Map<String,Long> map = r.countMountainHutsPerAltitudeRange();
		assertNotNull(map);
	}
	
	@Test
	public void testTotalBedNumberPerProvince() {
		Map<String, Integer> map = r.totalBedsNumberPerProvince();
		assertNotNull(map);
	}
}